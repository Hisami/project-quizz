// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

(function (modules, entry, mainEntry, parcelRequireName, globalName) {
  /* eslint-disable no-undef */
  var globalObject =
    typeof globalThis !== 'undefined'
      ? globalThis
      : typeof self !== 'undefined'
      ? self
      : typeof window !== 'undefined'
      ? window
      : typeof global !== 'undefined'
      ? global
      : {};
  /* eslint-enable no-undef */

  // Save the require from previous bundle to this closure if any
  var previousRequire =
    typeof globalObject[parcelRequireName] === 'function' &&
    globalObject[parcelRequireName];

  var cache = previousRequire.cache || {};
  // Do not use `require` to prevent Webpack from trying to bundle this call
  var nodeRequire =
    typeof module !== 'undefined' &&
    typeof module.require === 'function' &&
    module.require.bind(module);

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire =
          typeof globalObject[parcelRequireName] === 'function' &&
          globalObject[parcelRequireName];
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error("Cannot find module '" + name + "'");
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = (cache[name] = new newRequire.Module(name));

      modules[name][0].call(
        module.exports,
        localRequire,
        module,
        module.exports,
        this
      );
    }

    return cache[name].exports;

    function localRequire(x) {
      var res = localRequire.resolve(x);
      return res === false ? {} : newRequire(res);
    }

    function resolve(x) {
      var id = modules[name][1][x];
      return id != null ? id : x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [
      function (require, module) {
        module.exports = exports;
      },
      {},
    ];
  };

  Object.defineProperty(newRequire, 'root', {
    get: function () {
      return globalObject[parcelRequireName];
    },
  });

  globalObject[parcelRequireName] = newRequire;

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (mainEntry) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(mainEntry);

    // CommonJS
    if (typeof exports === 'object' && typeof module !== 'undefined') {
      module.exports = mainExports;

      // RequireJS
    } else if (typeof define === 'function' && define.amd) {
      define(function () {
        return mainExports;
      });

      // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }
})({"iJYvl":[function(require,module,exports) {
var global = arguments[3];
var HMR_HOST = null;
var HMR_PORT = null;
var HMR_SECURE = false;
var HMR_ENV_HASH = "d6ea1d42532a7575";
module.bundle.HMR_BUNDLE_ID = "5c1b77e3b71e74eb";
"use strict";
/* global HMR_HOST, HMR_PORT, HMR_ENV_HASH, HMR_SECURE, chrome, browser, globalThis, __parcel__import__, __parcel__importScripts__, ServiceWorkerGlobalScope */ /*::
import type {
  HMRAsset,
  HMRMessage,
} from '@parcel/reporter-dev-server/src/HMRServer.js';
interface ParcelRequire {
  (string): mixed;
  cache: {|[string]: ParcelModule|};
  hotData: mixed;
  Module: any;
  parent: ?ParcelRequire;
  isParcelRequire: true;
  modules: {|[string]: [Function, {|[string]: string|}]|};
  HMR_BUNDLE_ID: string;
  root: ParcelRequire;
}
interface ParcelModule {
  hot: {|
    data: mixed,
    accept(cb: (Function) => void): void,
    dispose(cb: (mixed) => void): void,
    // accept(deps: Array<string> | string, cb: (Function) => void): void,
    // decline(): void,
    _acceptCallbacks: Array<(Function) => void>,
    _disposeCallbacks: Array<(mixed) => void>,
  |};
}
interface ExtensionContext {
  runtime: {|
    reload(): void,
    getURL(url: string): string;
    getManifest(): {manifest_version: number, ...};
  |};
}
declare var module: {bundle: ParcelRequire, ...};
declare var HMR_HOST: string;
declare var HMR_PORT: string;
declare var HMR_ENV_HASH: string;
declare var HMR_SECURE: boolean;
declare var chrome: ExtensionContext;
declare var browser: ExtensionContext;
declare var __parcel__import__: (string) => Promise<void>;
declare var __parcel__importScripts__: (string) => Promise<void>;
declare var globalThis: typeof self;
declare var ServiceWorkerGlobalScope: Object;
*/ var OVERLAY_ID = "__parcel__error__overlay__";
var OldModule = module.bundle.Module;
function Module(moduleName) {
    OldModule.call(this, moduleName);
    this.hot = {
        data: module.bundle.hotData,
        _acceptCallbacks: [],
        _disposeCallbacks: [],
        accept: function(fn) {
            this._acceptCallbacks.push(fn || function() {});
        },
        dispose: function(fn) {
            this._disposeCallbacks.push(fn);
        }
    };
    module.bundle.hotData = undefined;
}
module.bundle.Module = Module;
var checkedAssets, acceptedAssets, assetsToAccept /*: Array<[ParcelRequire, string]> */ ;
function getHostname() {
    return HMR_HOST || (location.protocol.indexOf("http") === 0 ? location.hostname : "localhost");
}
function getPort() {
    return HMR_PORT || location.port;
} // eslint-disable-next-line no-redeclare
var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== "undefined") {
    var hostname = getHostname();
    var port = getPort();
    var protocol = HMR_SECURE || location.protocol == "https:" && !/localhost|127.0.0.1|0.0.0.0/.test(hostname) ? "wss" : "ws";
    var ws = new WebSocket(protocol + "://" + hostname + (port ? ":" + port : "") + "/"); // Web extension context
    var extCtx = typeof chrome === "undefined" ? typeof browser === "undefined" ? null : browser : chrome; // Safari doesn't support sourceURL in error stacks.
    // eval may also be disabled via CSP, so do a quick check.
    var supportsSourceURL = false;
    try {
        (0, eval)('throw new Error("test"); //# sourceURL=test.js');
    } catch (err) {
        supportsSourceURL = err.stack.includes("test.js");
    } // $FlowFixMe
    ws.onmessage = async function(event) {
        checkedAssets = {} /*: {|[string]: boolean|} */ ;
        acceptedAssets = {} /*: {|[string]: boolean|} */ ;
        assetsToAccept = [];
        var data = JSON.parse(event.data);
        if (data.type === "update") {
            // Remove error overlay if there is one
            if (typeof document !== "undefined") removeErrorOverlay();
            let assets = data.assets.filter((asset)=>asset.envHash === HMR_ENV_HASH); // Handle HMR Update
            let handled = assets.every((asset)=>{
                return asset.type === "css" || asset.type === "js" && hmrAcceptCheck(module.bundle.root, asset.id, asset.depsByBundle);
            });
            if (handled) {
                console.clear(); // Dispatch custom event so other runtimes (e.g React Refresh) are aware.
                if (typeof window !== "undefined" && typeof CustomEvent !== "undefined") window.dispatchEvent(new CustomEvent("parcelhmraccept"));
                await hmrApplyUpdates(assets);
                for(var i = 0; i < assetsToAccept.length; i++){
                    var id = assetsToAccept[i][1];
                    if (!acceptedAssets[id]) hmrAcceptRun(assetsToAccept[i][0], id);
                }
            } else fullReload();
        }
        if (data.type === "error") {
            // Log parcel errors to console
            for (let ansiDiagnostic of data.diagnostics.ansi){
                let stack = ansiDiagnostic.codeframe ? ansiDiagnostic.codeframe : ansiDiagnostic.stack;
                console.error("\uD83D\uDEA8 [parcel]: " + ansiDiagnostic.message + "\n" + stack + "\n\n" + ansiDiagnostic.hints.join("\n"));
            }
            if (typeof document !== "undefined") {
                // Render the fancy html overlay
                removeErrorOverlay();
                var overlay = createErrorOverlay(data.diagnostics.html); // $FlowFixMe
                document.body.appendChild(overlay);
            }
        }
    };
    ws.onerror = function(e) {
        console.error(e.message);
    };
    ws.onclose = function() {
        console.warn("[parcel] \uD83D\uDEA8 Connection to the HMR server was lost");
    };
}
function removeErrorOverlay() {
    var overlay = document.getElementById(OVERLAY_ID);
    if (overlay) {
        overlay.remove();
        console.log("[parcel] ✨ Error resolved");
    }
}
function createErrorOverlay(diagnostics) {
    var overlay = document.createElement("div");
    overlay.id = OVERLAY_ID;
    let errorHTML = '<div style="background: black; opacity: 0.85; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; font-family: Menlo, Consolas, monospace; z-index: 9999;">';
    for (let diagnostic of diagnostics){
        let stack = diagnostic.frames.length ? diagnostic.frames.reduce((p, frame)=>{
            return `${p}
<a href="/__parcel_launch_editor?file=${encodeURIComponent(frame.location)}" style="text-decoration: underline; color: #888" onclick="fetch(this.href); return false">${frame.location}</a>
${frame.code}`;
        }, "") : diagnostic.stack;
        errorHTML += `
      <div>
        <div style="font-size: 18px; font-weight: bold; margin-top: 20px;">
          🚨 ${diagnostic.message}
        </div>
        <pre>${stack}</pre>
        <div>
          ${diagnostic.hints.map((hint)=>"<div>\uD83D\uDCA1 " + hint + "</div>").join("")}
        </div>
        ${diagnostic.documentation ? `<div>📝 <a style="color: violet" href="${diagnostic.documentation}" target="_blank">Learn more</a></div>` : ""}
      </div>
    `;
    }
    errorHTML += "</div>";
    overlay.innerHTML = errorHTML;
    return overlay;
}
function fullReload() {
    if ("reload" in location) location.reload();
    else if (extCtx && extCtx.runtime && extCtx.runtime.reload) extCtx.runtime.reload();
}
function getParents(bundle, id) /*: Array<[ParcelRequire, string]> */ {
    var modules = bundle.modules;
    if (!modules) return [];
    var parents = [];
    var k, d, dep;
    for(k in modules)for(d in modules[k][1]){
        dep = modules[k][1][d];
        if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) parents.push([
            bundle,
            k
        ]);
    }
    if (bundle.parent) parents = parents.concat(getParents(bundle.parent, id));
    return parents;
}
function updateLink(link) {
    var newLink = link.cloneNode();
    newLink.onload = function() {
        if (link.parentNode !== null) // $FlowFixMe
        link.parentNode.removeChild(link);
    };
    newLink.setAttribute("href", link.getAttribute("href").split("?")[0] + "?" + Date.now()); // $FlowFixMe
    link.parentNode.insertBefore(newLink, link.nextSibling);
}
var cssTimeout = null;
function reloadCSS() {
    if (cssTimeout) return;
    cssTimeout = setTimeout(function() {
        var links = document.querySelectorAll('link[rel="stylesheet"]');
        for(var i = 0; i < links.length; i++){
            // $FlowFixMe[incompatible-type]
            var href = links[i].getAttribute("href");
            var hostname = getHostname();
            var servedFromHMRServer = hostname === "localhost" ? new RegExp("^(https?:\\/\\/(0.0.0.0|127.0.0.1)|localhost):" + getPort()).test(href) : href.indexOf(hostname + ":" + getPort());
            var absolute = /^https?:\/\//i.test(href) && href.indexOf(location.origin) !== 0 && !servedFromHMRServer;
            if (!absolute) updateLink(links[i]);
        }
        cssTimeout = null;
    }, 50);
}
function hmrDownload(asset) {
    if (asset.type === "js") {
        if (typeof document !== "undefined") {
            let script = document.createElement("script");
            script.src = asset.url + "?t=" + Date.now();
            if (asset.outputFormat === "esmodule") script.type = "module";
            return new Promise((resolve, reject)=>{
                var _document$head;
                script.onload = ()=>resolve(script);
                script.onerror = reject;
                (_document$head = document.head) === null || _document$head === void 0 || _document$head.appendChild(script);
            });
        } else if (typeof importScripts === "function") {
            // Worker scripts
            if (asset.outputFormat === "esmodule") return import(asset.url + "?t=" + Date.now());
            else return new Promise((resolve, reject)=>{
                try {
                    importScripts(asset.url + "?t=" + Date.now());
                    resolve();
                } catch (err) {
                    reject(err);
                }
            });
        }
    }
}
async function hmrApplyUpdates(assets) {
    global.parcelHotUpdate = Object.create(null);
    let scriptsToRemove;
    try {
        // If sourceURL comments aren't supported in eval, we need to load
        // the update from the dev server over HTTP so that stack traces
        // are correct in errors/logs. This is much slower than eval, so
        // we only do it if needed (currently just Safari).
        // https://bugs.webkit.org/show_bug.cgi?id=137297
        // This path is also taken if a CSP disallows eval.
        if (!supportsSourceURL) {
            let promises = assets.map((asset)=>{
                var _hmrDownload;
                return (_hmrDownload = hmrDownload(asset)) === null || _hmrDownload === void 0 ? void 0 : _hmrDownload.catch((err)=>{
                    // Web extension bugfix for Chromium
                    // https://bugs.chromium.org/p/chromium/issues/detail?id=1255412#c12
                    if (extCtx && extCtx.runtime && extCtx.runtime.getManifest().manifest_version == 3) {
                        if (typeof ServiceWorkerGlobalScope != "undefined" && global instanceof ServiceWorkerGlobalScope) {
                            extCtx.runtime.reload();
                            return;
                        }
                        asset.url = extCtx.runtime.getURL("/__parcel_hmr_proxy__?url=" + encodeURIComponent(asset.url + "?t=" + Date.now()));
                        return hmrDownload(asset);
                    }
                    throw err;
                });
            });
            scriptsToRemove = await Promise.all(promises);
        }
        assets.forEach(function(asset) {
            hmrApply(module.bundle.root, asset);
        });
    } finally{
        delete global.parcelHotUpdate;
        if (scriptsToRemove) scriptsToRemove.forEach((script)=>{
            if (script) {
                var _document$head2;
                (_document$head2 = document.head) === null || _document$head2 === void 0 || _document$head2.removeChild(script);
            }
        });
    }
}
function hmrApply(bundle, asset) {
    var modules = bundle.modules;
    if (!modules) return;
    if (asset.type === "css") reloadCSS();
    else if (asset.type === "js") {
        let deps = asset.depsByBundle[bundle.HMR_BUNDLE_ID];
        if (deps) {
            if (modules[asset.id]) {
                // Remove dependencies that are removed and will become orphaned.
                // This is necessary so that if the asset is added back again, the cache is gone, and we prevent a full page reload.
                let oldDeps = modules[asset.id][1];
                for(let dep in oldDeps)if (!deps[dep] || deps[dep] !== oldDeps[dep]) {
                    let id = oldDeps[dep];
                    let parents = getParents(module.bundle.root, id);
                    if (parents.length === 1) hmrDelete(module.bundle.root, id);
                }
            }
            if (supportsSourceURL) // Global eval. We would use `new Function` here but browser
            // support for source maps is better with eval.
            (0, eval)(asset.output);
             // $FlowFixMe
            let fn = global.parcelHotUpdate[asset.id];
            modules[asset.id] = [
                fn,
                deps
            ];
        } else if (bundle.parent) hmrApply(bundle.parent, asset);
    }
}
function hmrDelete(bundle, id) {
    let modules = bundle.modules;
    if (!modules) return;
    if (modules[id]) {
        // Collect dependencies that will become orphaned when this module is deleted.
        let deps = modules[id][1];
        let orphans = [];
        for(let dep in deps){
            let parents = getParents(module.bundle.root, deps[dep]);
            if (parents.length === 1) orphans.push(deps[dep]);
        } // Delete the module. This must be done before deleting dependencies in case of circular dependencies.
        delete modules[id];
        delete bundle.cache[id]; // Now delete the orphans.
        orphans.forEach((id)=>{
            hmrDelete(module.bundle.root, id);
        });
    } else if (bundle.parent) hmrDelete(bundle.parent, id);
}
function hmrAcceptCheck(bundle, id, depsByBundle) {
    if (hmrAcceptCheckOne(bundle, id, depsByBundle)) return true;
     // Traverse parents breadth first. All possible ancestries must accept the HMR update, or we'll reload.
    let parents = getParents(module.bundle.root, id);
    let accepted = false;
    while(parents.length > 0){
        let v = parents.shift();
        let a = hmrAcceptCheckOne(v[0], v[1], null);
        if (a) // If this parent accepts, stop traversing upward, but still consider siblings.
        accepted = true;
        else {
            // Otherwise, queue the parents in the next level upward.
            let p = getParents(module.bundle.root, v[1]);
            if (p.length === 0) {
                // If there are no parents, then we've reached an entry without accepting. Reload.
                accepted = false;
                break;
            }
            parents.push(...p);
        }
    }
    return accepted;
}
function hmrAcceptCheckOne(bundle, id, depsByBundle) {
    var modules = bundle.modules;
    if (!modules) return;
    if (depsByBundle && !depsByBundle[bundle.HMR_BUNDLE_ID]) {
        // If we reached the root bundle without finding where the asset should go,
        // there's nothing to do. Mark as "accepted" so we don't reload the page.
        if (!bundle.parent) return true;
        return hmrAcceptCheck(bundle.parent, id, depsByBundle);
    }
    if (checkedAssets[id]) return true;
    checkedAssets[id] = true;
    var cached = bundle.cache[id];
    assetsToAccept.push([
        bundle,
        id
    ]);
    if (!cached || cached.hot && cached.hot._acceptCallbacks.length) return true;
}
function hmrAcceptRun(bundle, id) {
    var cached = bundle.cache[id];
    bundle.hotData = {};
    if (cached && cached.hot) cached.hot.data = bundle.hotData;
    if (cached && cached.hot && cached.hot._disposeCallbacks.length) cached.hot._disposeCallbacks.forEach(function(cb) {
        cb(bundle.hotData);
    });
    delete bundle.cache[id];
    bundle(id);
    cached = bundle.cache[id];
    if (cached && cached.hot && cached.hot._acceptCallbacks.length) cached.hot._acceptCallbacks.forEach(function(cb) {
        var assetsToAlsoAccept = cb(function() {
            return getParents(module.bundle.root, id);
        });
        if (assetsToAlsoAccept && assetsToAccept.length) // $FlowFixMe[method-unbinding]
        assetsToAccept.push.apply(assetsToAccept, assetsToAlsoAccept);
    });
    acceptedAssets[id] = true;
}

},{}],"h7u1C":[function(require,module,exports) {
/* import images */ var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
var _etoUsagiKamifubukiPng = require("./image/eto_usagi_kamifubuki.png");
var _etoUsagiKamifubukiPngDefault = parcelHelpers.interopDefault(_etoUsagiKamifubukiPng);
var _etoUsagiBanzaiPng = require("./image/eto_usagi_banzai.png");
var _etoUsagiBanzaiPngDefault = parcelHelpers.interopDefault(_etoUsagiBanzaiPng);
var _etoUsagiKakizomePng = require("./image/eto_usagi_kakizome.png");
var _etoUsagiKakizomePngDefault = parcelHelpers.interopDefault(_etoUsagiKakizomePng);
var _木Png = require("./image/木.png");
var _木PngDefault = parcelHelpers.interopDefault(_木Png);
var _kiPng = require("./image/ki.png");
var _kiPngDefault = parcelHelpers.interopDefault(_kiPng);
var _森Png = require("./image/森.png");
var _森PngDefault = parcelHelpers.interopDefault(_森Png);
var _moriPng = require("./image/mori.png");
var _moriPngDefault = parcelHelpers.interopDefault(_moriPng);
var _川Png = require("./image/川.png");
var _川PngDefault = parcelHelpers.interopDefault(_川Png);
var _kqPng = require("./image/kq.png");
var _kqPngDefault = parcelHelpers.interopDefault(_kqPng);
var _目Png = require("./image/目.png");
var _目PngDefault = parcelHelpers.interopDefault(_目Png);
var _mePng = require("./image/me.png");
var _mePngDefault = parcelHelpers.interopDefault(_mePng);
var _魚Png = require("./image/魚.png");
var _魚PngDefault = parcelHelpers.interopDefault(_魚Png);
var _sakanaPng = require("./image/sakana.png");
var _sakanaPngDefault = parcelHelpers.interopDefault(_sakanaPng);
var _休みPng = require("./image/休み.png");
var _休みPngDefault = parcelHelpers.interopDefault(_休みPng);
var _yasumiPng = require("./image/yasumi.png");
var _yasumiPngDefault = parcelHelpers.interopDefault(_yasumiPng);
var _困Png = require("./image/困.png");
var _困PngDefault = parcelHelpers.interopDefault(_困Png);
var _イラスト2Png = require("./image/イラスト2.png");
var _イラスト2PngDefault = parcelHelpers.interopDefault(_イラスト2Png);
/*  section start */ const startBtn = document.querySelector("#startbtn");
const start = document.querySelector("#start");
const play = document.querySelector("#play");
const skipBtn = document.querySelector(".skip");
let demonstration1 = document.querySelector("#demonstration1");
let demonstration2 = document.querySelector("#demonstration2");
let demonstration3 = document.querySelector("#demonstration3");
let shortCut = false;
/** 
* Une fonction qui add/remove "display-none" entre deux facteur
*@param demo1 un element <section> dans un body de HTML
*@param demo2 un element <section> dans un body de HTML
* @returns ajouter "d-none" au class de demo1 et rejouter "d-none" de class de demo2
*/ function nextDemo(demo1, demo2) {
    demo1.classList.add("d-none");
    demo2.classList.remove("d-none");
}
/* pour changer un section apparu en display avec click sur les buttons */ skipBtn?.addEventListener("click", ()=>{
    shortCut = true;
    nextDemo(demonstration1, demonstration3);
});
/* animation avec setTimeout */ setTimeout(()=>{
    if (shortCut == false) nextDemo(demonstration1, demonstration2);
}, 7000);
setTimeout(()=>{
    if (shortCut == false) nextDemo(demonstration2, demonstration3);
}, 12000);
/* pour changer un section apparu en display avec click sur les buttons */ startBtn?.addEventListener("click", ()=>{
    nextDemo(start, play);
});
/*  section play */ const question = document.querySelector("#question");
const qimage = document.querySelector("#qimage");
const answers = document.querySelector("#answers");
const chat = document.querySelector(".chat");
const mask = document.querySelector("#mask");
const showExplication = document.querySelector("#explication");
const eimage = document.querySelector("#eimage");
const explicationtext = document.querySelector("#explicationtext");
const showScore = document.querySelector("#showScore");
const scoreResult = document.querySelector("#scoreResult");
const returnToquizz = document.querySelector("#returnToquiz");
const simage = document.querySelector("#simage");
const message = document.querySelector("#message");
const indice = document.querySelector("#indice");
const indiceBtn = document.querySelector("#indiceBtn");
const divIndice = document.querySelector(".divIndice");
const divChatting = document.querySelector(".chatting");
let currentNum = 0;
let score = 0;
let isAnswered = false;
/* tableau de object qui stocke (1,question:string), (2,image of question:img), (3,list of answers:string[]),(4,bonne reponse:string),(5,image of explication:img),(6,explication:string),(7,indice:string) */ const quizSet = [
    {
        question: "Que signifie ce kanji ?",
        qimage: (0, _木PngDefault.default),
        answers: [
            "Arbre",
            "Maison",
            "Humain"
        ],
        bonreponse: "Arbre",
        eimage: (0, _kiPngDefault.default),
        explication: "origine de 木(KI):arbre ",
        indice: "De la forme d'une plante"
    },
    {
        question: "Que signifie ce kanji ?",
        qimage: (0, _森PngDefault.default),
        answers: [
            "Montagne",
            "For\xeat",
            "F\xeate"
        ],
        bonreponse: "For\xeat",
        eimage: (0, _moriPngDefault.default),
        explication: "origine de 森(MORI):for\xeat",
        indice: "木=arbre,donc avec plusieurs arbres..?"
    },
    {
        question: "Que signifie ce kanji ?",
        qimage: (0, _川PngDefault.default),
        answers: [
            "Riv\xe8re",
            "Vent",
            "Lumi\xe8re"
        ],
        bonreponse: "Riv\xe8re",
        eimage: (0, _kqPngDefault.default),
        explication: "origine de 川(KAWA):riv\xe8re",
        indice: "\xe7a exprime le mouvement de.."
    },
    {
        question: "Que signifie ce kanji ?",
        qimage: (0, _目PngDefault.default),
        answers: [
            "Lune",
            "Fen\xeatre",
            "Oeil"
        ],
        bonreponse: "Oeil",
        eimage: (0, _mePngDefault.default),
        explication: "origine de 目(M\xc9):oeil",
        indice: "tourner le Kanji de 90 degr\xe9s..."
    },
    {
        question: "Que signifie ce kanji ?",
        qimage: (0, _魚PngDefault.default),
        answers: [
            "Poisson",
            "Bateau",
            "Cabine"
        ],
        bonreponse: "Poisson",
        eimage: (0, _sakanaPngDefault.default),
        explication: "origine de 魚(SAKANA):poisson ",
        indice: "Puisque qu'il a des ar\xeates.."
    },
    {
        question: "Que signifie ce kanji ?",
        qimage: (0, _休みPngDefault.default),
        answers: [
            "Marcher",
            "Se reposer",
            "Travailler"
        ],
        bonreponse: "Se reposer",
        eimage: (0, _yasumiPngDefault.default),
        explication: "origine de 休む(YASU(MU)):se reposer ",
        indice: "人=un homme , qui est \xe0 c\xf4t\xe9 de 木=arbre.."
    },
    {
        question: "Que signifie ce kanji ?",
        qimage: (0, _困PngDefault.default),
        answers: [
            "\xcatre heureux",
            "\xcatre en bonne sant\xe9",
            "\xcatre en difficult\xe9"
        ],
        bonreponse: "\xcatre en difficult\xe9",
        eimage: (0, _イラスト2PngDefault.default),
        explication: "origine de 困る(KOMA(RU)):\xeatre en difficult\xe9",
        indice: "Un arbre entour\xe9 de barri\xe8res"
    }
];
setQuizz();
toNext();
/** 
* setQuizz():Une fonction qui affiche chaque element de quizz dans le tableau (quizSet) sur le html et selon le choix clicked appele une autre functions pour le judger 
* @returns affiche "un question","un image of the question", "choices of the question" , "explication of the question", "indice of the question", selon currentNum et appele checkAnswer()
avec "click" sur un choix("li");
*/ function setQuizz() {
    if (quizSet[currentNum]) {
        isAnswered = false;
        if (question) question.textContent = quizSet[currentNum].question;
        if (qimage) qimage.src = quizSet[currentNum].qimage;
        const li = document.querySelectorAll("li");
        for(let i = 0; i < li.length; i++){
            li[i].textContent = quizSet[currentNum].answers[i];
            li[i].addEventListener("click", ()=>{
                chat.style.transform = "rotate(30deg)";
                checkAnswer(li[i]);
                setTimeout(()=>{
                    explication(currentNum);
                }, 1000);
                divIndice?.classList.add("d-none");
            });
        }
        if (indice) indice.textContent = quizSet[currentNum].indice;
        if (currentNum == quizSet.length - 1 && returnToquizz) returnToquizz.textContent = "Show score!!";
    } else return console.log("termine");
}
/** 
* checkAnswer():Une fonction qui selon un paramètre vérifier le réponse et affiche un text
*@li :element HTML<li> comme un choix de réponse choisi avec click 
*@returns  Si un paramètre <li> est égale avec bonne reponse("bonreponse") de objet de currentNum dans un tableau, envoye un text "Correct" , sinon envoye "wrong" et et change "isAnswered " "true".
*/ function checkAnswer(li) {
    while(isAnswered != true)if (li.textContent == quizSet[currentNum].bonreponse) {
        li.textContent = "Correct!!";
        li.classList.add("correct");
        score++;
        isAnswered = true;
    } else {
        li.textContent = "Faux!!";
        li.classList.add("wrong");
        isAnswered = true;
    }
}
/** 
* checkAnswer():Une fonction qui permet de afficher le modal "explication" 
*@num :number qui correspond avec "currentNum"
*@returns  rejouter le class "d-none" de "mask(div)" et "showExplication" pour les afficher sur écran et
afficher l'image et le text de explication selon le paramètre;  
*/ function explication(num) {
    mask?.classList.remove("d-none");
    showExplication?.classList.remove("d-none");
    eimage.src = quizSet[num].eimage;
    explicationtext.textContent = quizSet[num].explication;
}
/* avec event "click", afficher le contnu de indice */ indiceBtn?.addEventListener("click", ()=>{
    divChatting?.classList.remove("d-none");
});
/** 
* toNext():rejouter les classes et préparer pour se déplacer un question suivant, et à la fin de quiz affiche un résultat et un commentaire.
*@returns  avec action click sur le button("returnToquizz"),rejouter les changements pendant le question et les classes "correct"/"wrong" de l'answer clicked et ajouter 1 currentNum. 
Si c'était le deniere question, afficher un image et un text sur showScore<div> et l'afficher selon "scoreResult".
*/ function toNext() {
    if (returnToquizz && answers) returnToquizz.addEventListener("click", ()=>{
        chat.style.transform = "rotate(0deg)";
        showExplication?.classList.add("d-none");
        divIndice?.classList.remove("d-none");
        divChatting?.classList.add("d-none");
        mask?.classList.add("d-none");
        if (currentNum == quizSet.length - 1) {
            divIndice?.classList.add("d-none");
            mask?.classList.remove("d-none");
            showScore?.classList.remove("d-none");
            if (scoreResult) scoreResult.textContent = `${score}/${quizSet.length}`;
            if (score == quizSet.length && message && simage) {
                simage.src = (0, _etoUsagiKamifubukiPngDefault.default);
                message.textContent = "G\xe9nial, tu es un ma\xeetre des kanjis!!";
            } else if (score >= Math.ceil(quizSet.length / 2) && message && simage) {
                simage.src = (0, _etoUsagiBanzaiPngDefault.default);
                message.textContent = "Pas mal, tu as une bonne intuition";
            } else if (message && simage) {
                simage.src = (0, _etoUsagiKakizomePngDefault.default);
                message.textContent = "Dommage, essaye \xe0 nouveau";
            }
        }
        for(let i = 0; i < 3; i++)answers.children[i].classList.remove("correct", "wrong");
        currentNum++;
        setQuizz();
    });
}

},{"./image/eto_usagi_kamifubuki.png":"kq1Pk","./image/eto_usagi_banzai.png":"6DGCi","./image/eto_usagi_kakizome.png":"eMieG","./image/木.png":"l8klP","./image/ki.png":"lTpnO","./image/森.png":"2cJ9L","./image/mori.png":"6ZUPY","./image/川.png":"g8Wlk","./image/kq.png":"iKCdV","./image/目.png":"jXNxa","./image/me.png":"cdXMv","./image/魚.png":"jCRKm","./image/sakana.png":"kDXs8","./image/休み.png":"1Mbje","./image/yasumi.png":"ca3Vv","./image/困.png":"f3Lv3","./image/イラスト2.png":"gu6Ap","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"kq1Pk":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "eto_usagi_kamifubuki.223de438.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"lgJ39":[function(require,module,exports) {
"use strict";
var bundleURL = {};
function getBundleURLCached(id) {
    var value = bundleURL[id];
    if (!value) {
        value = getBundleURL();
        bundleURL[id] = value;
    }
    return value;
}
function getBundleURL() {
    try {
        throw new Error();
    } catch (err) {
        var matches = ("" + err.stack).match(/(https?|file|ftp|(chrome|moz|safari-web)-extension):\/\/[^)\n]+/g);
        if (matches) // The first two stack frames will be this function and getBundleURLCached.
        // Use the 3rd one, which will be a runtime in the original bundle.
        return getBaseURL(matches[2]);
    }
    return "/";
}
function getBaseURL(url) {
    return ("" + url).replace(/^((?:https?|file|ftp|(chrome|moz|safari-web)-extension):\/\/.+)\/[^/]+$/, "$1") + "/";
} // TODO: Replace uses with `new URL(url).origin` when ie11 is no longer supported.
function getOrigin(url) {
    var matches = ("" + url).match(/(https?|file|ftp|(chrome|moz|safari-web)-extension):\/\/[^/]+/);
    if (!matches) throw new Error("Origin not found");
    return matches[0];
}
exports.getBundleURL = getBundleURLCached;
exports.getBaseURL = getBaseURL;
exports.getOrigin = getOrigin;

},{}],"6DGCi":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "eto_usagi_banzai.bd41e282.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"eMieG":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "eto_usagi_kakizome.cdb9cc9b.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"l8klP":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "木.50bf22a2.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"lTpnO":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "ki.abc8466f.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"2cJ9L":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "森.cdfedafc.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"6ZUPY":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "mori.774d79e6.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"g8Wlk":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "川.68f8dd2d.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"iKCdV":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "kq.4dce15d4.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"jXNxa":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "目.3b7ae583.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"cdXMv":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "me.d8fb09ba.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"jCRKm":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "魚.8695e951.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"kDXs8":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "sakana.7a014183.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"1Mbje":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "休み.1c01fb7f.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"ca3Vv":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "yasumi.60ae856d.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"f3Lv3":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "困.454e0971.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"gu6Ap":[function(require,module,exports) {
module.exports = require("./helpers/bundle-url").getBundleURL("7UhFu") + "イラスト2.8dc51761.png" + "?" + Date.now();

},{"./helpers/bundle-url":"lgJ39"}],"gkKU3":[function(require,module,exports) {
exports.interopDefault = function(a) {
    return a && a.__esModule ? a : {
        default: a
    };
};
exports.defineInteropFlag = function(a) {
    Object.defineProperty(a, "__esModule", {
        value: true
    });
};
exports.exportAll = function(source, dest) {
    Object.keys(source).forEach(function(key) {
        if (key === "default" || key === "__esModule" || dest.hasOwnProperty(key)) return;
        Object.defineProperty(dest, key, {
            enumerable: true,
            get: function() {
                return source[key];
            }
        });
    });
    return dest;
};
exports.export = function(dest, destName, get) {
    Object.defineProperty(dest, destName, {
        enumerable: true,
        get: get
    });
};

},{}]},["iJYvl","h7u1C"], "h7u1C", "parcelRequire529b")

//# sourceMappingURL=index.b71e74eb.js.map
