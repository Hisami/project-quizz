/* start */ const start = document.querySelector("#start");
const play = document.querySelector("#play");
const demo = document.querySelector("#demo");
const startbtn = document.querySelector("#startbtn");
start?.addEventListener("click", ()=>{
    start?.classList.add("d-none");
    play?.classList.remove("d-none");
});
demo?.addEventListener("click", ()=>{
    start?.classList.add("d-none");
    demo?.classList.remove("d-none");
});
/* play */ const question = document.querySelector("#question");
const qimage = document.querySelector("#qimage");
const answers = document.querySelector("#answers");
const button = document.querySelector("#button");
const mask = document.querySelector("#mask");
const showScore = document.querySelector("#showScore");
const scoreResult = document.querySelector("#scoreResult");
const simage = document.querySelector("#simage");
const message = document.querySelector("#message");
const returnButton = document.querySelector("#returnButton");
const quizSet = [
    {
        question: "Qu'est ce que c'est A?",
        qimage: "https://images.unsplash.com/photo-1514888286974-6c03e2ca1dba?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2886&q=80",
        answers: [
            "AO",
            "A1",
            "A2"
        ],
        bonreponse: "AO"
    },
    {
        question: "What is B?",
        qimage: "https://images.unsplash.com/photo-1519052537078-e6302a4968d4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2670&q=80",
        answers: [
            "BO",
            "B1",
            "B2"
        ],
        bonreponse: "B2"
    },
    {
        question: "What is C?",
        qimage: "https://images.unsplash.com/photo-1491485880348-85d48a9e5312?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2670&q=80",
        answers: [
            "CO",
            "C1",
            "C2"
        ],
        bonreponse: "C2"
    }
];
let currentNum = 0;
let score = 0;
let isAnswered = false;
function setQuizz() {
    isAnswered = false;
    if (question) question.textContent = quizSet[currentNum].question;
    if (qimage) qimage.src = quizSet[currentNum].qimage;
    const li = document.querySelectorAll("li");
    for(let i = 0; i < 3; i++){
        li[i].textContent = quizSet[currentNum].answers[i];
        li[i].addEventListener("click", ()=>{
            checkAnswer(li[i]);
        });
    }
    if (currentNum == quizSet.length - 1 && button) button.textContent = "Show score!!";
}
setQuizz();
toNext();
function checkAnswer(li) {
    while(isAnswered != true)if (li.textContent == quizSet[currentNum].bonreponse) {
        li.textContent = "Correct!!";
        li.classList.add("correct");
        score++;
        isAnswered = true;
        if (button) button.classList.remove("disabled");
    } else {
        li.textContent = "Wrong!!";
        li.classList.add("wrong");
        isAnswered = true;
    }
    if (button) button.classList.remove("disabled");
}
function toNext() {
    if (button && answers) button.addEventListener("click", ()=>{
        if (currentNum == quizSet.length - 1) {
            console.log(`Your score is ${score}/${quizSet.length}`);
            mask?.classList.remove("d-none");
            showScore?.classList.remove("d-none");
            if (scoreResult) scoreResult.textContent = `${score}/${quizSet.length}`;
            if (score == quizSet.length && message && simage) {
                simage.src = "../image/eto_usagi_banzai.png";
                message.textContent = "Great!!";
            } else if (score >= Math.ceil(quizSet.length / 2) && message && simage) {
                simage.src = "https://images.unsplash.com/photo-1514888286974-6c03e2ca1dba?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2886&q=80";
                message.textContent = "Not bad";
            } else if (message && simage) {
                simage.src = "https://images.unsplash.com/photo-1514888286974-6c03e2ca1dba?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2886&q=80";
                message.textContent = "Try again";
            }
        }
        for(let i = 0; i < 3; i++)answers.children[i].classList.remove("correct", "wrong");
        button.classList.add("disabled");
        currentNum++;
        setQuizz();
    });
} /* demonstration */ 

//# sourceMappingURL=index.377278e2.js.map
