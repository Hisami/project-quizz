# project-quizz
https://kanji-quizz.netlify.app/#play
## Name
Kanji quizz

## Description
Ce quizz a pour but de vous aider à apprendre les kanjis (idéogrammes japonais) en devinant leur signification grâce à leur forme et des indices. Après chaque question, je vous révèle l'origine du kanji et son évolution à travers le temps.


## Visuals
<version mobile>
![1AD69D19-662F-4C3A-8D09-D416D6D4FE2F_1_105_c](/uploads/1d9116e01ff06077e43d310298984436/1AD69D19-662F-4C3A-8D09-D416D6D4FE2F_1_105_c.jpeg)
<version pc>
![7FE8F488-7920-43ED-BAE7-B49E866C87AF_1_105_c](/uploads/93185afa51647d168460a35fc06dc610/7FE8F488-7920-43ED-BAE7-B49E866C87AF_1_105_c.jpeg)


## Usage
1. Opening: introduction au quizz avec une explication sur l'origine des kanjis. Vous pouvez passer cette séquence en appuyant sur "skip" pour commencer le quizz. 
2. Pour chaque question, vous avez le choix entre 3 réponses possibles. Vous pouvez cliquer sur l'icône "indice" en bas à gauche de l'écran pour obtenir une aide. En cas de réponse de correcte, la case choisie devient verte. En cas d'erreur, la case devient rouge. Vient ensuite automatiquement une explication du kanji et son évolution au cours du temps. 
Vous pouvez passer à la prochaine question en cliquant sur "Kanji suivant".
A la fin du quizz, suivant votre score, vous obtenez un grade et un petit texte d'encouragement.
3. En appuyant sur "restart" vous pouvez recommencer le quizz depuis le début.



## Support
En cas de question:
*email:hisamistolz@gmail.com 
*gitlab: @Hisami 

## Authors and acknowledgment
Author:Hisami Stolz




