/* import images */
import great from "./image/eto_usagi_kamifubuki.png"
import notbad from "./image/eto_usagi_banzai.png"
import fight from "./image/eto_usagi_kakizome.png"
import tree from "./image/木.png"
import treeEx from "./image/ki.png"
import forest from "./image/森.png"
import forestEx from "./image/mori.png"
import river from "./image/川.png"
import riverEx from "./image/kq.png"
import eye from "./image/目.png"
import eyeEx from "./image/me.png"
import fish from "./image/魚.png"
import fishEx from "./image/sakana.png"
import reposer from "./image/休み.png"
import reposerEx from "./image/yasumi.png"
import trouble from "./image/困.png"
import troubleEx from "./image/イラスト2.png"


/*  section start */
const startBtn = document.querySelector<HTMLButtonElement>("#startbtn")
const start = document.querySelector<HTMLElement>("#start")
const play = document.querySelector<HTMLElement>("#play")
const skipBtn = document.querySelector<HTMLButtonElement>(".skip")
let demonstration1 = document.querySelector<HTMLElement>("#demonstration1");
let demonstration2 = document.querySelector<HTMLElement>("#demonstration2");
let demonstration3 = document.querySelector<HTMLElement>("#demonstration3");
let shortCut: boolean = false;

/** 
* Une fonction qui add/remove "display-none" entre deux facteur
*@param demo1 un element <section> dans un body de HTML
*@param demo2 un element <section> dans un body de HTML
* @returns ajouter "d-none" au class de demo1 et rejouter "d-none" de class de demo2
*/
function nextDemo(demo1: any, demo2: any) {
    demo1.classList.add("d-none");
    demo2.classList.remove("d-none")
}

/* pour changer un section apparu en display avec click sur les buttons */
skipBtn?.addEventListener('click', () => {
    shortCut = true;
    nextDemo(demonstration1, demonstration3)
})

/* animation avec setTimeout */
setTimeout(() => {
    if (shortCut == false) {
        nextDemo(demonstration1, demonstration2)
    }
}, 7000);
setTimeout(() => {
    if (shortCut == false) {
        nextDemo(demonstration2, demonstration3)
    }
}, 12000);

/* pour changer un section apparu en display avec click sur les buttons */
startBtn?.addEventListener('click', () => {
    nextDemo(start, play)
})


/*  section play */
const question: any = document.querySelector<HTMLElement>("#question");
const qimage: any = document.querySelector<HTMLElement>("#qimage");
const answers: any = document.querySelector<HTMLElement>("#answers");
const chat: any = document.querySelector<HTMLElement>(".chat");
const mask = document.querySelector<HTMLElement>("#mask");
const showExplication = document.querySelector<HTMLElement>("#explication");
const eimage: any = document.querySelector<HTMLImageElement>("#eimage");
const explicationtext: any = document.querySelector<HTMLElement>("#explicationtext")
const showScore = document.querySelector<HTMLElement>("#showScore");
const scoreResult = document.querySelector<HTMLElement>("#scoreResult");
const returnToquizz = document.querySelector<HTMLElement>("#returnToquiz")
const simage: any = document.querySelector<HTMLImageElement>("#simage");
const message = document.querySelector<HTMLElement>("#message");
const indice: any = document.querySelector<HTMLElement>("#indice");
const indiceBtn = document.querySelector<HTMLButtonElement>("#indiceBtn");
const divIndice = document.querySelector<HTMLElement>(".divIndice");
const divChatting = document.querySelector<HTMLElement>(".chatting")
let currentNum: number = 0;
let score: number = 0;
let isAnswered: boolean = false;

/* tableau de object qui stocke (1,question:string), (2,image of question:img), (3,list of answers:string[]),(4,bonne reponse:string),(5,image of explication:img),(6,explication:string),(7,indice:string) */
const quizSet = [
    { question: "Que signifie ce kanji ?", qimage: tree, answers: ['Arbre', 'Maison', 'Humain'], bonreponse: 'Arbre', eimage: treeEx, explication: "origine de 木(KI):arbre ", indice: "De la forme d'une plante" },

    { question: "Que signifie ce kanji ?", qimage: forest, answers: ['Montagne', 'Forêt', 'Fête'], bonreponse: 'Forêt', eimage: forestEx, explication: "origine de 森(MORI):forêt", indice: "木=arbre,donc avec plusieurs arbres..?" },

    { question: "Que signifie ce kanji ?", qimage: river, answers: ['Rivère', 'Vent', 'Lumière'], bonreponse: 'Rivère', eimage: riverEx, explication: "origine de 川(KAWA):rivère", indice: "ça exprime le mouvement de.." },

    { question: "Que signifie ce kanji ?", qimage: eye, answers: ['Lune', 'Fenêtre', 'Oeil'], bonreponse: 'Oeil', eimage: eyeEx, explication: "origine de 目(MÉ):oeil", indice: "tourner le Kanji de 90 degrés..." },

    { question: "Que signifie ce kanji ?", qimage: fish, answers: ['Poisson', 'Bateau', 'Cabine'], bonreponse: 'Poisson', eimage: fishEx, explication: "origine de 魚(SAKANA):poisson ", indice: "Puisque qu'il a des arêtes.." },

    { question: "Que signifie ce kanji ?", qimage: reposer, answers: ['Marcher', 'Se reposer', 'Travailler'], bonreponse: 'Se reposer', eimage: reposerEx, explication: "origine de 休む(YASU(MU)):se reposer ", indice: "人=un homme , qui est à côté de 木=arbre.." },

    { question: "Que signifie ce kanji ?", qimage: trouble, answers: ['Être heureux', 'Être en bonne santé', 'Être en difficulté'], bonreponse: 'Être en difficulté', eimage: troubleEx, explication: "origine de 困る(KOMA(RU)):être en difficulté", indice: "Un arbre entouré de barrières" },

]

setQuizz();
toNext()

/** 
* setQuizz():Une fonction qui affiche chaque element de quizz dans le tableau (quizSet) sur le html et selon le choix clicked appele une autre functions pour le judger 
* @returns affiche "un question","un image of the question", "choices of the question" , "explication of the question", "indice of the question", selon currentNum et appele checkAnswer()
avec "click" sur un choix("li");
*/

function setQuizz() {
    if (quizSet[currentNum]) {
        isAnswered = false;
        if (question) {
            question.textContent = quizSet[currentNum].question;
        }
        if (qimage) {
            qimage.src = quizSet[currentNum].qimage;
        }
        const li: any = document.querySelectorAll("li");
        for (let i = 0; i < li.length; i++) {
            li[i].textContent = quizSet[currentNum].answers[i];
            li[i].addEventListener('click', () => {
                chat.style.transform = "rotate(30deg)";
                checkAnswer(li[i]);
                setTimeout(() => { explication(currentNum) }, 1000)
                divIndice?.classList.add("d-none");
            });
        };
        if (indice) {
            indice.textContent = quizSet[currentNum].indice;
        }
        if (currentNum == quizSet.length - 1 && returnToquizz) {
            returnToquizz.textContent = "Show score!!"
        }
    }
    else {
        return console.log("termine")
    }

}


/** 
* checkAnswer():Une fonction qui selon un paramètre vérifier le réponse et affiche un text
*@li :element HTML<li> comme un choix de réponse choisi avec click 
*@returns  Si un paramètre <li> est égale avec bonne reponse("bonreponse") de objet de currentNum dans un tableau, envoye un text "Correct" , sinon envoye "wrong" et et change "isAnswered " "true".
*/

function checkAnswer(li: any) {
    while (isAnswered != true) {
        if (li.textContent == quizSet[currentNum].bonreponse) {
            li.textContent = "Correct!!";
            li.classList.add("correct");
            score++
            isAnswered = true;
        }
        else {
            li.textContent = "Faux!!";
            li.classList.add("wrong");
            isAnswered = true;
        }
    }
}


/** 
* checkAnswer():Une fonction qui permet de afficher le modal "explication" 
*@num :number qui correspond avec "currentNum"
*@returns  rejouter le class "d-none" de "mask(div)" et "showExplication" pour les afficher sur écran et
afficher l'image et le text de explication selon le paramètre;  
*/
function explication(num: number) {
    mask?.classList.remove("d-none");
    showExplication?.classList.remove("d-none");
    eimage.src = quizSet[num].eimage;
    explicationtext.textContent = quizSet[num].explication;
}

/* avec event "click", afficher le contnu de indice */
indiceBtn?.addEventListener('click', () => {
    divChatting?.classList.remove("d-none");
})


/** 
* toNext():rejouter les classes et préparer pour se déplacer un question suivant, et à la fin de quiz affiche un résultat et un commentaire.
*@returns  avec action click sur le button("returnToquizz"),rejouter les changements pendant le question et les classes "correct"/"wrong" de l'answer clicked et ajouter 1 currentNum. 
Si c'était le deniere question, afficher un image et un text sur showScore<div> et l'afficher selon "scoreResult".
*/
function toNext() {
    if (returnToquizz && answers) {
        returnToquizz.addEventListener('click', () => {
            chat.style.transform = "rotate(0deg)";
            showExplication?.classList.add("d-none")
            divIndice?.classList.remove("d-none");
            divChatting?.classList.add("d-none");
            mask?.classList.add("d-none");
            if (currentNum == quizSet.length - 1) {
                divIndice?.classList.add("d-none");
                mask?.classList.remove("d-none");
                showScore?.classList.remove("d-none")
                if (scoreResult) {
                    scoreResult.textContent = `${score}/${quizSet.length}`
                }
                if (score == quizSet.length && message && simage) {
                    simage.src = great;
                    message.textContent = "Génial, tu es un maître des kanjis!!";
                } else if (score >= Math.ceil(quizSet.length / 2) && message && simage) {
                    simage.src = notbad;
                    message.textContent = "Pas mal, tu as une bonne intuition";
                } else if (message && simage) {
                    simage.src = fight;
                    message.textContent = "Dommage, essaye à nouveau";
                }
            }
            for (let i = 0; i < 3; i++) {
                answers.children[i].classList.remove("correct", "wrong");
            }
            currentNum++;
            setQuizz();
        });
    }
}





